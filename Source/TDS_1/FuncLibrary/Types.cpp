// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TDS_1.h"
#include "Interface/TDS_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UTDS_1StateEffect> AddEffectClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UTDS_1StateEffect* myEffect = Cast<UTDS_1StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = true;
					if (!myEffect->bIsStakable)
					{
						int8 j = 0;
						TArray<UTDS_1StateEffect*> CurrentEffects;
						ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						//ERROR: ���� ����� ������� ��������, ���������� ����������!	
						//if (CurrentEffects.Num() > 0)
						//{
						//	while (j < CurrentEffects.Num() && !bIsCanAddEffect)
						//	{
						//		if (CurrentEffects[j]->GetClass() != AddEffectClass)
						//		{
						//			bIsCanAddEffect = true;
						//		}
						//		j++;
						//	}
						//}
						//else
						//{
						//	bIsCanAddEffect = true;
						//}
						while (j < CurrentEffects.Num() && bIsCanAddEffect)
						{
							if (CurrentEffects[j]->GetClass() == AddEffectClass)
							{
								bIsCanAddEffect = false;
							}
							j++;
						}

					}
					//else
					//{
					//	bIsCanAddEffect = true;
					//}

					if (bIsCanAddEffect)
					{

						UTDS_1StateEffect* NewEffect = NewObject<UTDS_1StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}
					}

				}
				i++;
			}
		}

	}

}
