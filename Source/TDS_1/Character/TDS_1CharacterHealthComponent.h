// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TDS_1HealthComponent.h"
#include "TDS_1CharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TDS_1_API UTDS_1CharacterHealthComponent : public UTDS_1HealthComponent
{
	GENERATED_BODY()
	
public:
	UTDS_1CharacterHealthComponent();

	UPROPERTY(Category = "Shield", EditAnywhere, BlueprintReadOnly)
		UStaticMesh* ShieldBody = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
		UMaterialInterface* DamageDecal = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Shield")
		UMaterialInterface* ShieldMaterial = nullptr;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

protected:

	float Shield = 100.0f;

	virtual void BeginPlay() override;
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FHitResult HitInfo;

	virtual void ChangeHealthValue(float ChangeValue) override;

	float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();
};
