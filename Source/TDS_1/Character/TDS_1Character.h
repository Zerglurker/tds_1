// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FuncLibrary/Types.h"
#include "Weapons/WeaponDefault.h" 
#include "Character/TDS_1InventoryComponent.h"
#include "Character/TDS_1CharacterHealthComponent.h"
#include "Interface/TDS_IGameActor.h"
#include "StateEffects/TDS_1StateEffect.h"

#include "TDS_1Character.generated.h"

UCLASS(Blueprintable)
class ATDS_1Character : public ACharacter, public ITDS_IGameActor
{
	GENERATED_BODY()

public:
	ATDS_1Character();

	FTimerHandle TimerHandle_RagDollTimer;

	void BeginPlay() override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* NewInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_1InventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDS_1CharacterHealthComponent* CharHealthComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	void SmoothCamHeight(float DeltaTime);

	void UpdateStamina(float DeltaTime);

	float DefAccelerate = 0.f;

	//new cursor
	UDecalComponent* CurrentCursor = nullptr;

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Cursor")
		FVector CursorSize = FVector(20.f,40.f,40.f);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float MinHeight = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float MaxHeight = 1200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
		float Height = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float StagesHeight = 7.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float CoffSmooth = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		float SpeedHeight = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bIsAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
		float SprintWeakness = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float Stamina = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxStamina = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTDS_1StateEffect> AbilityEffect;

	//Weapon   
	AWeaponDefault* CurrentWeapon = nullptr;

	//Shield
	UPROPERTY(Category = "Shield", EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* ShieldBody = nullptr;

	//Effect
	TArray<UTDS_1StateEffect*> Effects;

	//Inputs hooks
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	UFUNCTION()
		void InputHeight(float Value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;
	float SensetiveHeight = 1.0f;
	float HeightControl = 0.0f;

	//Tick for movement character
	UFUNCTION()
		void MovementTick(float DeltaTime);

	//Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)//VisualOnly
		void RemoveCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	//Inventory Func
	void TrySwicthNextWeapon();
	void TrySwitchPreviosWeapon();
	//ability func
	void TryAbilityEnabled();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	//Interface ITDS_IGameActor {
	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_1StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_1StateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_1StateEffect* newEffect);

	virtual UStaticMeshComponent* GetShieldBody();
	//Interface ITDS_IGameActor }

	UFUNCTION()
		void CharDead();
	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

};

