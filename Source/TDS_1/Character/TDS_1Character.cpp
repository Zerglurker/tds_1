// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TDS_1Character.h"

#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SceneComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

#include "Game/TDS_1GameInstance.h"
#include "Weapons/Projectiles/ProjectileDefault.h"

ATDS_1Character::ATDS_1Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true;//SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTDS_1InventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_1Character::InitWeapon);
	}

	CharHealthComponent  = CreateDefaultSubobject<UTDS_1CharacterHealthComponent>(TEXT("HealthComponent"));
	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATDS_1Character::CharDead);
		
		//TODO: ����� ���������� ������ ����������
		ShieldBody = CreateDefaultSubobject <UStaticMeshComponent>(TEXT("ShieldBody"));
		if (ShieldBody){
			ShieldBody->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			ShieldBody->SetupAttachment(RootComponent);// AttachToComponent(RootComponent, Rule,);
			ShieldBody->SetStaticMesh(CharHealthComponent->ShieldBody);
			ShieldBody->SetMaterial(0, CharHealthComponent->ShieldMaterial);
			//ShieldBody->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
			ShieldBody->SetRelativeTransform(FTransform(FRotator(),FVector(0.f, 0.f, -88.f),FVector(2.f)), false, nullptr, ETeleportType::ResetPhysics);
		}
	}

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	//Validate height
	if (MinHeight > MaxHeight) {
		float h = MinHeight;
		MaxHeight = MinHeight;
		MinHeight = h;
	}
	Height = HeightControl = MaxHeight;
	SensetiveHeight = (MaxHeight - MinHeight) / StagesHeight;
	if (SensetiveHeight < 1) SensetiveHeight = 1.0f;

	//Validate Speed
	if (MovementInfo.RunSpeed > MovementInfo.SprintSpeed) {
		MovementInfo.SprintSpeed = MovementInfo.RunSpeed + 1;
	}
	DefAccelerate = GetCharacterMovement()->MaxAcceleration;
}

void ATDS_1Character::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//Camera height on mouse wheel
	CameraBoom->TargetArmLength = Height; //ControlHeight;

	//new cursor move
	if (CurrentCursor) {
		if (APlayerController* PC = Cast<APlayerController>(GetController())) {
			FHitResult HitResult;

			PC->GetHitResultUnderCursor(ECC_Visibility, true, HitResult);
			FVector CursorFV = HitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CurrentCursor->SetWorldLocation(HitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDS_1Character::BeginPlay() {
	Super::BeginPlay();

//	InitWeapon(InitWeaponName);

	if (CursorMaterial) {
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDS_1Character::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_1Character::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_1Character::InputAxisY);

	NewInputComponent->BindAxis(TEXT("MouseWheel"), this, &ATDS_1Character::InputHeight);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDS_1Character::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDS_1Character::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_1Character::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDS_1Character::TrySwicthNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDS_1Character::TrySwitchPreviosWeapon);

	NewInputComponent->BindAction(TEXT("AblityAction"), EInputEvent::IE_Pressed, this, &ATDS_1Character::TryAbilityEnabled);
}

void ATDS_1Character::InputAxisX(float Value)
{
	//OFF:Sprint only Forward
	//if (!SprintEnabled) {
	AxisX = Value;
	//}
}

void ATDS_1Character::InputAxisY(float Value)
{
	//OFF:Sprint only Forward
	//if (!SprintEnabled) {
	AxisY = Value;
	//}
}

void ATDS_1Character::InputAttackPressed()
{
	AttackCharEvent(true);
}

void ATDS_1Character::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDS_1Character::InputHeight(float Value)
{
	if (Value >= 0) {
		if (MaxHeight > (HeightControl + Value * SensetiveHeight))
			HeightControl += Value * SensetiveHeight;
		else HeightControl = MaxHeight;
	}
	else {
		if (MinHeight < (HeightControl + Value * SensetiveHeight))
			HeightControl += Value * SensetiveHeight;
		else
			HeightControl = MinHeight;
	}
}

void ATDS_1Character::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		//from BP_PlayerControl
		AddMovementInput(FVector(1.0f, 0, 0), AxisX);
		AddMovementInput(FVector(0, 1.0f, 0), AxisY);

		//Change rotation on sprint
		if (SprintEnabled) {
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();
			SetActorRotation((FQuat(myRotator)));
		}
		else {
			//from TopDownCharacter: look at mouse cursor
			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (myController) {

				FHitResult HitResult;
				//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6,false, HitResult);
				//myController->GetHitResultUnderCursor(ECC_EngineTraceChannel1, true, HitResult);
				myController->GetHitResultUnderCursor(ECC_Visibility, true, HitResult);

				FRotator myRotator = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location);
				SetActorRotation(FQuat(FRotator(0, myRotator.Yaw, 0)));

				//DEBUG
				FString msg = FString::Printf(TEXT("look to mouse %s"), *HitResult.Location.ToString());
				GEngine->AddOnScreenDebugMessage(4, 5.f, FColor::Orange, msg);

				//����������� ������ �� �����������
				if (CurrentWeapon) {
					FVector Displacement = FVector(0);
					switch (MovementState)
					{
					case EMovementState::Aim_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					case EMovementState::AimWalk_State:
						CurrentWeapon->ShouldReduceDispersion = true;
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::Sprint_State:
						break;
					default:
						break;
					}

					CurrentWeapon->ShootEndLocation = HitResult.Location + Displacement;

				}
			}
		}

	}
	//Smooth Height cam
	SmoothCamHeight(DeltaTime);
	//Control Stamina
	UpdateStamina(DeltaTime);

}

void ATDS_1Character::SmoothCamHeight(float DeltaTime) {

	if (Height < HeightControl) {
		Height += DeltaTime * SpeedHeight;
		if (Height > HeightControl) Height = HeightControl;
	}
	else if (Height > HeightControl) {
		Height -= DeltaTime * SpeedHeight;
		if (Height < HeightControl) Height = HeightControl;
	}
}

void ATDS_1Character::UpdateStamina(float DeltaTime) {
	//Stamina
	//���� �������� ���� ����, �������� ����������� ������� � ���������� ��������
	float ResSpeed = GetCharacterMovement()->MaxWalkSpeed;
	if (ResSpeed > MovementInfo.RunSpeed) {

		if (Stamina == 0) {
			ResSpeed = MovementInfo.RunSpeed;
		}

		if (Stamina - SprintWeakness * DeltaTime > 0)
			Stamina -= SprintWeakness * DeltaTime;
		else {
			Stamina = 0;
			if (MovementState == EMovementState::Sprint_State) {
				MovementState = EMovementState::Run_State;
				SprintEnabled = false;
				CharacterUpdate();
			}
		}
	}
	else {
		if ((Stamina + MaxStamina / 100 * DeltaTime) < MaxStamina)
			Stamina += MaxStamina / 100 * DeltaTime;
		else
			Stamina = MaxStamina;
	}
}

void ATDS_1Character::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDS_1Character::AttackCharEvent - CurrentWeapon -NULL"));
}

//CALLED: BP_Character ChangeMovementState
//  on keypress
void ATDS_1Character::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::AimWalk_State:
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Sprint_State:
		//���������
		GetCharacterMovement()->MaxAcceleration = MovementInfo.SprintAgility;
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	case EMovementState::Run_State:
		//���������
		GetCharacterMovement()->MaxAcceleration = DefAccelerate;
		ResSpeed = MovementInfo.RunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDS_1Character::ChangeMovementState()
{
	//��������� ������, ������ ���� ���� ������� �������
	if (SprintEnabled && (Stamina >= MaxStamina / 4)) {
		AimEnabled = false;
		WalkEnabled = false;
		MovementState = EMovementState::Sprint_State;
	}
	else {
		if (AimEnabled) {
			if (WalkEnabled) {
				MovementState = EMovementState::AimWalk_State;
			}
			else {
				MovementState = EMovementState::Aim_State;
			}
		}
		else {
			if (WalkEnabled) {
				MovementState = EMovementState::Walk_State;
			}
			else {
				MovementState = EMovementState::Run_State;
			}
		}
	}

	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* curWeapon = GetCurrentWeapon();
	if (curWeapon)
	{
		curWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault * ATDS_1Character::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDS_1Character::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDS_1GameInstance* myGI = Cast<UTDS_1GameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;

	if (myGI) {
		//DEBUG
		//FString msg = IdWeaponName.ToString();
		//GEngine->AddOnScreenDebugMessage(0, 5.f, FColor::Orange, msg);

		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo)) {
			if (myWeaponInfo.WeaponClass) //InitWeaponClass
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;//GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->WeaponInfo = WeaponAdditionalInfo;
					//if(InventoryComponent)
					CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					//Not Forget remove delegate on change/drop weapon
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_1Character::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_1Character::WeaponReloadEnd);
					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_1Character::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
			else {
				//UE_LOG(LogTemp, Warning, TEXT("ATDS_1Character::InitWeapon - Weapon class %s"),myWeaponInfo.WeaponClass);
				UE_LOG(LogTemp, Warning, TEXT("ATDS_1Character::InitWeapon - Weapon class ''"));
			}

		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("ATDS_1Character::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void ATDS_1Character::RemoveCurrentWeapon()
{
}

void ATDS_1Character::TryReloadWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound)
			CurrentWeapon->InitReload();
	}

}

void ATDS_1Character::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_1Character::WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}


void ATDS_1Character::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDS_1Character::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void ATDS_1Character::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);

	WeaponFireStart_BP(Anim);
}

void ATDS_1Character::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

UDecalComponent * ATDS_1Character::GetCursorToWorld()
{
	return CurrentCursor;
}

//ToDO in one func TrySwitchPreviosWeapon && TrySwicthNextWeapon
//need Timer to Switch with Anim, this method stupid i must know switch success for second logic inventory
//now we not have not success switch/ if 1 weapon switch to self
void ATDS_1Character::TrySwicthNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATDS_1Character::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->WeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void ATDS_1Character::TryAbilityEnabled()
{
	if (AbilityEffect)//TODO Cool down
	{
		UTDS_1StateEffect* NewEffect = NewObject<UTDS_1StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}

EPhysicalSurface ATDS_1Character::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					UPhysicalMaterial* physMaterial = myMaterial->GetPhysicalMaterial();
					Result = physMaterial->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UTDS_1StateEffect*> ATDS_1Character::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_1Character::RemoveEffect(UTDS_1StateEffect * RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_1Character::AddEffect(UTDS_1StateEffect * newEffect)
{
	Effects.Add(newEffect);
}

UStaticMeshComponent * ATDS_1Character::GetShieldBody()
{
	return ShieldBody;
}

void ATDS_1Character::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	UnPossessed();

	//Timer rag doll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_1Character::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);
}

void ATDS_1Character::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATDS_1Character::TakeDamage(float DamageAmount, FDamageEvent const & DamageEvent, AController * EventInstigator, AActor * DamageCauser)
{
	float res;
	res = Super::TakeDamage(DamageAmount,DamageEvent,EventInstigator,DamageCauser);

	if (bIsAlive)
	{
		if (DamageEvent.GetTypeID() == 1) {
			UE_LOG(LogTemp, Display, TEXT("ATDS_1Character::TakeDamage - Point id")); //ElogVerbosity::Log
		}
		else if (DamageEvent.IsOfType(FPointDamageEvent::ClassID)) { //Point
			FPointDamageEvent PointDmg = *((FPointDamageEvent*)&DamageEvent);
			UE_LOG(LogTemp, Display, TEXT("ATDS_1Character::TakeDamage - Point")); 
		}
		else if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID)) { //Radial
			FRadialDamageEvent RadialDmg = *((FRadialDamageEvent const*)(&DamageEvent));
			UE_LOG(LogTemp, Display, TEXT("ATDS_1Character::TakeDamage - Radial"));
		}
		//FHitResult HitInfo;
		FVector ImpulseDir;
		DamageEvent.GetBestHitInfo(this, EventInstigator, CharHealthComponent->HitInfo, ImpulseDir);

		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}

	return res;
}
