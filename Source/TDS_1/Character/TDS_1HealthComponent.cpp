// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_1HealthComponent.h"

// Sets default values for this component's properties
UTDS_1HealthComponent::UTDS_1HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDS_1HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDS_1HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UTDS_1HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDS_1HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
	if (Health > 100.0f)
		Health = 100.0f;
	else if (Health <= 0.0f) {

		OnDead.Broadcast();
		DeadEvent_BP();

	}

}

void UTDS_1HealthComponent::ChangeHealthValue(float ChangeValue)
{
	//ChangeValue = ChangeValue;
	if(!IsInvulnerable || IsInvulnerable && ChangeValue>0)
		Health += ChangeValue * CoefDamage;

	if (Health > 100.0f)
		Health = 100.0f;
	else if (Health <= 0.0f) {

		OnDead.Broadcast();
		DeadEvent_BP();

	}

	OnHealthChange.Broadcast(Health, ChangeValue);

}

void UTDS_1HealthComponent::DeadEvent_BP_Implementation()
{
	// in BP
}

