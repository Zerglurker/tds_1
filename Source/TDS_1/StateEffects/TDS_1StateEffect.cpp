// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_1StateEffect.h"

#include "Kismet/GameplayStatics.h"

#include "Character/TDS_1HealthComponent.h"
#include "Interface/TDS_IGameActor.h"

bool UTDS_1StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	//BP
	InitObject_BP(Actor);

	return true;
}

void UTDS_1StateEffect::DestroyObject()
{

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}
	//BP
	DestroyObject_BP();

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}

}

void UTDS_1StateEffect::InitObject_BP_Implementation(AActor * Actor)
{
	
}

void UTDS_1StateEffect::DestroyObject_BP_Implementation()
{
}

bool UTDS_1StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UTDS_1StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTDS_1StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDS_1HealthComponent* myHealthComp = Cast<UTDS_1HealthComponent>(myActor->GetComponentByClass(UTDS_1HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}

bool UTDS_1StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_1StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_1StateEffect_ExecuteTimer::Execute, RateTime, true);

	if (ParticleEffect)
	{
		FName NameBoneToAttached;
		FVector Loc = FVector(0);

		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	}

	return true;
}

void UTDS_1StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTDS_1StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UTDS_1HealthComponent* myHealthComp = Cast<UTDS_1HealthComponent>(myActor->GetComponentByClass(UTDS_1HealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
