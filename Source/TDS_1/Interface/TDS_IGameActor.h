// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Engine/EngineTypes.h"
#include "StateEffects/TDS_1StateEffect.h"
#include "TDS_IGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTDS_IGameActor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_1_API ITDS_IGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual EPhysicalSurface GetSurfaceType();

	virtual TArray<UTDS_1StateEffect*> GetAllCurrentEffects();
	virtual void RemoveEffect(UTDS_1StateEffect* RemoveEffect);
	virtual void AddEffect(UTDS_1StateEffect* newEffect);

	virtual UStaticMeshComponent* GetShieldBody();
};
