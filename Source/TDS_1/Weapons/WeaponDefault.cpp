// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "Engine/World.h"
#include "Engine/Engine.h"
#include "Character/TDS_1InventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
	//DropShellLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropShellLocation"));
	//DropShellLocation->SetupAttachment(RootComponent);
	//DropClipLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("DropClipLocation"));
	//DropClipLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
	ClipDropTick(DeltaTime);
	ShellDropTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{
			if (FireTimer < 0.f) {
					Fire();
			}
			else
				FireTimer -= DeltaTime;
	}
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading){
		if (ReloadTimer < 0.0f) {

			FinishReload();
		}
		else
			ReloadTimer -= DeltaTime;
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}

		if (CurrentDispersion < CurrentDispersionMin)
		{

			CurrentDispersion = CurrentDispersionMin;

		}
		else
		{
			if (CurrentDispersion > CurrentDispersionMax)
			{
				CurrentDispersion = CurrentDispersionMax;
			}
		}
	}
	//if (ShowDebug)
	//	UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::ClipDropTick(float DeltaTime)
{
	if (DropClipFlag)
	{
		if (DropClipTimer < 0.0f)
		{
			DropClipFlag = false;
			FTransform  DropTransform = SkeletalMeshWeapon->GetSocketTransform(TEXT("AmmoEject"), ERelativeTransformSpace::RTS_Component);

			FString msg = FString::Printf(TEXT("MagazineDrop to %s"), *DropTransform.ToString());
			GEngine->AddOnScreenDebugMessage(6, 5.f, FColor::Orange, msg);

			InitDropMesh(WeaponSetting.MagazineDrop.DropMesh, DropTransform,                             FVector(0.0f, 0.0f, -1.0f),                    WeaponSetting.MagazineDrop.DropMeshLifeTime, WeaponSetting.MagazineDrop.ImpulseRandomDispersion, WeaponSetting.MagazineDrop.PowerImpulse, WeaponSetting.MagazineDrop.CustomMass);
//			InitDropMesh(WeaponSetting.ClipDropMesh.DropMesh, WeaponSetting.ClipDropMesh.DropMeshOffset, WeaponSetting.ClipDropMesh.DropMeshImpulseDir, WeaponSetting.ClipDropMesh.DropMeshLifeTime, WeaponSetting.ClipDropMesh.ImpulseRandomDispersion, WeaponSetting.ClipDropMesh.PowerImpulse, WeaponSetting.ClipDropMesh.CustomMass);
		}
		else
			DropClipTimer -= DeltaTime;
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			FTransform  ShellTransform = SkeletalMeshWeapon->GetSocketTransform(TEXT("AmmoEject"), ERelativeTransformSpace::RTS_Component);
			ShellTransform.SetRotation(FQuat(0.0f, 0.0f, -90.0f, 0.0f));

			FString msg = FString::Printf(TEXT("Shell eject to %s"), *ShellTransform.ToString());
			GEngine->AddOnScreenDebugMessage(6, 5.f, FColor::Orange, msg);
			//																							                                                60.0f, 45.0f, 100.0f, .0f
			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, ShellTransform,                            FVector(0.0f, 1.0f, 0.0f),                     WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
//			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, WeaponSetting.ShellBullets.DropMeshOffset, WeaponSetting.ShellBullets.DropMeshImpulseDir, WeaponSetting.ShellBullets.DropMeshLifeTime, WeaponSetting.ShellBullets.ImpulseRandomDispersion, WeaponSetting.ShellBullets.PowerImpulse, WeaponSetting.ShellBullets.CustomMass);
		}
		else
			DropShellTimer -= DeltaTime;
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh) {
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh()) {
		StaticMeshWeapon->DestroyComponent(true);
	}

	UpdateStateWeapon(EMovementState::Run_State);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else {
		WeaponFiring = false;
		FireTimer = 0.01f;//!!!!!
	}
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	//Anim
	UAnimMontage* AnimToPlay = nullptr;
	if (WeaponAiming)
		AnimToPlay = WeaponSetting.AnimCharFireAim;
	else
		AnimToPlay = WeaponSetting.AnimCharFire;
	
	if (WeaponSetting.AnimWeaponFire && SkeletalMeshWeapon) {
		UAnimInstance* AnimInstance = nullptr;
		AnimInstance = SkeletalMeshWeapon->GetAnimInstance();
		if(AnimInstance)
			AnimInstance->Montage_Play(WeaponSetting.AnimWeaponFire);
	}

	OnWeaponFireStart.Broadcast(AnimToPlay);

	//Fire logic
	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.Round = WeaponInfo.Round - 1;
	ChangeDispersion();

	//Move to AnimWeaponFire
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	int8 NumberProjectile = GetNumberProjectileByShot();

	if (ShootLocation)
	{
		FVector  SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{
			EndLocation = GetFireEndLocation();

			FVector Dir = EndLocation - SpawnLocation;

			Dir.Normalize();

			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = myMatrix.Rotator();
			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner      = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					//Done Init Projectile settings by id in table row(or keep in weapon table)
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
				}
			}
			else
			{
				//
				FHitResult Hit;
				TArray<AActor*> Actors;
				UKismetSystemLibrary::LineTraceSingle(GetWorld(),
					SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, true,
					Actors, EDrawDebugTrace::ForDuration,
					Hit, true);
				if (ShowDebug) {
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector()*WeaponSetting.DistanceTrace, FColor::Blue, false, 6.0f, (uint8)'\000', 0.5f);
				}

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid()) {
					EPhysicalSurface surfaceType = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(surfaceType)) {

						UMaterialInterface* decalMaterial = WeaponSetting.ProjectileSetting.HitDecals[surfaceType];
						if (decalMaterial && Hit.GetComponent()) {
							UGameplayStatics::SpawnDecalAttached(decalMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition);
						}

					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(surfaceType)) {
						UParticleSystem* hitParticle = WeaponSetting.ProjectileSetting.HitFXs[surfaceType];
						if (hitParticle) {
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), hitParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint,FVector(1.0f)));
						}
					}

					if (WeaponSetting.ProjectileSetting.HitSound) {
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.HitSound, Hit.ImpactPoint);
					}

					UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, surfaceType);

					//if (Hit.GetActor()->GetClass()->ImplementsInterface(UTPS_IGameActor::StaticClass()))
					//{
					//	//ITPS_IGameActor::Execute_AviableForEffects(Hit.GetActor());
					//	//ITPS_IGameActor::Execute_AviableForEffectsBP(Hit.GetActor());
					//}

					UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
				}
			
				//if (GetWorld()->LineTraceSingleByChannel(Hit, SpawnLocation, EndLocation * WeaponSetting.DistanceTrace, ECollisionChannel::ECC_Visibility)) {
				//}
			}
		}
	}

	//drop shells
	if (WeaponSetting.ShellBullets.DropMesh) {
		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			FTransform  ShellTransform = SkeletalMeshWeapon->GetSocketTransform(TEXT("AmmoEject"), ERelativeTransformSpace::RTS_Component);
			ShellTransform.SetRotation(FQuat(0.0f, 0.0f, -90.0f, 0.0f));

			FString msg = FString::Printf(TEXT("Shell eject to %s"), *ShellTransform.ToString());
			GEngine->AddOnScreenDebugMessage(6, 5.f, FColor::Orange, msg);

			InitDropMesh(WeaponSetting.ShellBullets.DropMesh, ShellTransform, FVector(0.0f, 1.0f, 0.0f), 60.0f, 45.0f, 100.0f, .0f);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}

	if (GetWeaponRound() <= 0 && !WeaponReloading)
	{
		//Init Reload
		if(CheckCanWeaponReload())
			InitReload();
	}

}

void AWeaponDefault::InitDropMesh(UStaticMesh *DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTime, float ImpulseDispersion, float ImpulsePower, float CustomMass)
{
	if (DropMesh) {

		FVector localDirection = this->GetActorForwardVector()*Offset.GetLocation().X 
			+ this->GetActorRightVector()*Offset.GetLocation().Y 
			+ this->GetActorUpVector()*Offset.GetLocation().Z;


		FTransform Transform;
		Transform.SetLocation(this->GetActorLocation() + localDirection);
		Transform.SetScale3D(Offset.GetScale3D());
		Transform.SetRotation((this->GetActorRotation() + Offset.Rotator()).Quaternion());

		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		SpawnParams.Owner = this;
		
		AStaticMeshActor* dropActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, SpawnParams);

		if (dropActor && dropActor->GetStaticMeshComponent()) {
			//settings / ��������
			dropActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			dropActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			dropActor->SetActorTickEnabled(false);
			dropActor->InitialLifeSpan = LifeTime;

			dropActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			dropActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			dropActor->GetStaticMeshComponent()->SetStaticMesh(DropMesh);

			dropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			dropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			dropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			dropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			dropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			dropActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);

			if (CustomMass > 0.0f) {
				dropActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, CustomMass, true);
			}

			if (!DropImpulseDirection.IsNearlyZero()) {
				FVector finalDirection;
				finalDirection = localDirection + (DropImpulseDirection * 1000.0f);
				if (!FMath::IsNearlyZero(ImpulseDispersion)) {
					finalDirection = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(finalDirection, ImpulseDispersion);
				}
				finalDirection.GetSafeNormal(0.0001f);

				dropActor->GetStaticMeshComponent()->AddImpulse(finalDirection * ImpulsePower);
			}
		}

	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
 	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:

		CurrentDispersionMax    = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin    = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		WeaponAiming = true;
		break;
	case EMovementState::AimWalk_State:

		CurrentDispersionMax    = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin    = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionReduction;
		WeaponAiming = true;
		break;
	case EMovementState::Walk_State:

		CurrentDispersionMax    = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMax;
		CurrentDispersionMin    = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Walk_StateDispersionReduction;
		WeaponAiming = false;
		break;
	case EMovementState::Run_State:

		CurrentDispersionMax    = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMax;
		CurrentDispersionMin    = WeaponSetting.DispersionWeapon.Run_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Run_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Run_StateDispersionReduction;
		WeaponAiming = false;
		break;
	case EMovementState::Sprint_State:
		BlockFire = true;
		WeaponAiming = false;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersion()
{  //����������� ������� � ������ ���������
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());
	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistanceTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}

	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
	}

	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return WeaponInfo.Round;
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;
	ReloadTimer = WeaponSetting.ReloadTime;

	//Character->blsAim
	if (WeaponAiming) {
		if (WeaponSetting.AnimCharReloadAim) //?AnimWeaponReload  
			OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReloadAim);
	}
	else
		if (WeaponSetting.AnimCharReload) //?AnimWeaponReload  
			OnWeaponReloadStart.Broadcast(WeaponSetting.AnimCharReload);

	//drop magazine
	if (WeaponSetting.MagazineDrop.DropMesh) {
		DropClipFlag = true;
		DropClipTimer = WeaponSetting.MagazineDrop.DropMeshTime;
	}
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	//WeaponInfo.Round = WeaponSetting.MaxRound;
	//OnWeaponReloadEnd.Broadcast();
	int32 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int8 AmmoNeedTakeFromInv;
	int8 NeedToReload = WeaponSetting.MaxRound - WeaponInfo.Round;

	if (NeedToReload > AviableAmmoFromInventory)
	{
		if (AviableAmmoFromInventory < 0) 
			AviableAmmoFromInventory = 0; 
		WeaponInfo.Round = AviableAmmoFromInventory;
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		WeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}

	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	OnWeaponReloadEnd.Broadcast(false, 0);
	DropClipFlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UTDS_1InventoryComponent* MyInv = Cast<UTDS_1InventoryComponent>(GetOwner()->GetComponentByClass(UTDS_1InventoryComponent::StaticClass()));
		if (MyInv)
		{
			int32 AviableAmmoForWeapon;
			if (!MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				result = false;
			}
		}
	}

	return result;
}

int32 AWeaponDefault::GetAviableAmmoForReload()
{
	int32 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UTDS_1InventoryComponent* MyInv = Cast<UTDS_1InventoryComponent>(GetOwner()->GetComponentByClass(UTDS_1InventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->CheckAmmoForWeapon(WeaponSetting.WeaponType, AviableAmmoForWeapon))
			{
				AviableAmmoForWeapon = AviableAmmoForWeapon;
			}
		}
	}
	return AviableAmmoForWeapon;
}

